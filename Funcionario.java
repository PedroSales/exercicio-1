public class Funcionario{



	private String nome;
	private String departamento;
	private double salario;
	private Data dataEntrada;
	private String rg;



	public void mostra(){

	System.out.println("Nome do Funcionário: " + this.nome + "\n");
	System.out.print("Salario: " + this.salario + "\n");
	System.out.print("Data de entrada: " +this.dataEntrada.formatada() + "\n");
	System.out.print("Departamento: " + this.departamento + "\n");
	System.out.print("RG: " + this.rg + "\n");
	System.out.print("Ganho anual: " + this.calculaGanhoAnual() + "\n");

	}

	public void setNome (String nome){
	   this.nome = nome;
	}
	
	public String getNome (){
		return nome;
	}

	public void setDepartamento(String departamento){
		this.departamento = departamento; 
	}

	public String getDepartamento(){
		return departamento;
	}

	public void setSalario (double salario){
		this.salario = salario;
	}

	public double getSalario(){
		return salario;	
	}

	public void setDataEntrada(Data dataEntrada){
		this.dataEntrada = dataEntrada;
	}

	public Data getDataEntrada(){
		return dataEntrada;
	}

	public void setRg (String rg){
		this.rg = rg;
	}

	public String getRg (){	
		return rg;
	}

	public void recebeAumento (double valorAumento){
	   this.salario = this.salario + valorAumento;   
	}	
	
	public void recebeAumentoPercent (int porcentagem){
		if(porcentagem>0 && porcentagem<100){
	   this.salario = this.salario + (this.salario * (porcentagem/100));
		}
		else{
		System.out.print("Valor invalido!");
		}

	}
	public double calculaGanhoAnual(){
		double ganhoAnual;
		ganhoAnual = this.salario * 12;
		return ganhoAnual;
	}
	
}

